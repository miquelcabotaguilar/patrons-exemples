package designpatrons.Builder;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        MealBuilder mealBuilder=new MealBuilder();
        Meal m1 = mealBuilder.prepareVegMenu();
        Meal m2 = mealBuilder.prepareChickenMenu();
    }
}

class MealBuilder{
    Meal prepareVegMenu(){
        Meal m = new Meal();
        m.addItem(new VegBurger());
        m.addItem(new Trina());
        return m;
    }
    Meal prepareChickenMenu(){
        Meal m = new Meal();
        m.addItem(new ChickenBurger());
        m.addItem(new Coke());
        return m;
    }
}

class Meal{
    private List<Item> items = new ArrayList<>();
    void addItem(Item item){
        items.add(item);
    }

    double getCost(){
        return items
                .stream()
                .map((i)->i.getPrice())
                .reduce((a,b)-> b+a)
                .get();
    }
}

interface Item{
    String getName();
    double getPrice();
    Pack getPack();
}

interface Pack{
    String pack();
}

class Wrapper implements Pack{
    @Override
    public String pack() {
        return "Wrapper";
    }
}

abstract class Burger implements Item{
    @Override
    public Pack getPack() {
        return new Wrapper();
    }
}
abstract class Drink implements Item{
    @Override
    public Pack getPack() {
        return new Bottle();
    }
}

class ChickenBurger extends Burger{
    @Override
    public String getName() {
        return "Chiken Burger";
    }

    @Override
    public double getPrice() {
        return 3.55;
    }
}

class VegBurger extends Burger{
    @Override
    public String getName() {
        return "Vegan Burger";
    }

    @Override
    public double getPrice() {
        return 4.55;
    }
}

class Bottle implements Pack{

    @Override
    public String pack() {
        return "Bottle";
    }
}

class Coke extends Drink {

    @Override
    public String getName() {
        return "Coke";
    }

    @Override
    public double getPrice() {
        return 2.10;
    }
}

class Trina extends Drink {

    @Override
    public String getName() {
        return "Trina";
    }

    @Override
    public double getPrice() {
        return 2.00;
    }
}