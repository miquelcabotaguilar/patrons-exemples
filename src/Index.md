# Patrons de Disseny

## Singleton

Singleton es un patrón de diseño creacional que nos permite asegurarnos de que una clase tenga una única instancia, 
a la vez que proporciona un punto de acceso global a dicha instancia.

El patrón Singleton resuelve dos problemas al mismo tiempo, vulnerando el Principio de responsabilidad única

* Garantizar que una clase tenga una única instancia. ¿Por qué querría alguien controlar cuántas
 instancias tiene una clase? El motivo más habitual es controlar el acceso a algún recurso compartido,
 por ejemplo, una base de datos o un archivo.

* Proporcionar un punto de acceso global a dicha instancia. ¿Recuerdas esas variables globales que utilizaste 
 (bueno, sí, fui yo) para almacenar objetos esenciales? Aunque son muy útiles, también son poco seguras, ya que 
 cualquier código podría sobrescribir el contenido de esas variables y descomponer la aplicación.
 
Todas las implementaciones del patrón Singleton tienen estos dos pasos en común:


*  Hacer privado el constructor por defecto para evitar que otros objetos utilicen el operador
  new con la clase Singleton.

* Crear un método de creación estático que actúe como constructor. Tras bambalinas,este método invoca al 
  constructor privado para crear un objeto y lo guarda en un campo estático. Las siguientes 
  llamadas a este método devuelven el objeto almacenado en caché.

## DAO

El patrón Data Access Object (DAO) pretende principalmente independizar la aplicación de la forma de acceder a la base 
de datos, o cualquier otro tipo de repositorio de datos. Para ello se centraliza el código relativo al acceso al 
repositorio de datos en las clases llamadas DAO



## MVC

MVC era inicialmente un patrón arquitectural, un modelo o guía que expresa cómo organizar y estructurar los componentes 
de un sistema software, sus responsabilidades y las relaciones existentes entre cada uno de ellos.

MVC, parte de las iniciales de Modelo-Vista-Controlador (Model-View-Controller, en inglés), que son las capas o grupos 
de componentes en los que organizaremos nuestras aplicaciones bajo este paradigma.

MODEL -> Dades "de Negocí"

CONTROLADOR -> Intermediari

VISTA -> Objecte que s'encarrega de presentar les dades del programa

## Builder
Builder es un patrón de diseño creacional que nos permite construir objetos complejos paso a paso. El patrón nos 
permite producir distintos tipos y representaciones de un objeto empleando el mismo código de construcción.

El patrón Builder sugiere que saques el código de construcción del objeto de su propia clase y lo coloques dentro 
de objetos independientes llamados constructores.
El patrón organiza la construcción de objetos en una serie de pasos (construirParedes, construirPuerta, etc.).
Para crear un objeto, se ejecuta una serie de estos pasos en un objeto constructor. Lo importante es que no necesitas 
invocar todos los pasos. Puedes invocar sólo aquellos que sean necesarios para producir una configuración particular 
de un objeto.

Puede ser que algunos pasos de la construcción necesiten una implementación diferente cuando tengamos que construir 
distintas representaciones del producto. Por ejemplo, las paredes de una cabaña pueden ser de madera, pero las paredes
de un castillo tienen que ser de piedra.

## Interceptor

El patrón de filtro interceptor se utiliza para realizar un procesamiento previo o posterior de la solicitud o 
respuesta de la aplicación. Defina los filtros y aplíquelos a la solicitud antes de pasarla a la aplicación de destino 
real. El filtro puede hacer un registro de autenticación / autorización / grabación, o rastrear la solicitud, y luego
pasar la solicitud al programa de procesamiento correspondiente. Las siguientes son las entidades de este patrón de 
diseño.
Filtrar 

* El filtro realiza ciertas tareas antes o después de que el manejador de solicitudes ejecute la solicitud.
Cadena de filtro 

* La cadena de filtros tiene múltiples filtros y ejecuta estos filtros en Target en el orden definido.
Target 

* El objeto Target es el manejador de solicitudes.
Administrador de filtros 

* Gestor de filtros para gestionar filtros y cadenas de filtros.
Cliente (Cliente) 

* El cliente es el objeto que envía la solicitud al objeto de destino.
