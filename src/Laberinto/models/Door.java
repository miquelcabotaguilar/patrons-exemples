package Laberinto.models;

import java.util.List;

public class Door implements Mapside {
    private static boolean open = false;

    public Door(Room r1, Room r2) {

    }

    @Override
    public void enter(Player player) {
        if(!this.open){
            List<Item> items=player.getItemList();
            items.stream()
                    .filter(i -> i instanceof Key)
                    .map(i->(Key) i)
                    .forEach(k -> k.open(this));
        }if(this.open){
            Room r = getOtherRoom(player.getCurrentRoom());
            player.setCurrentRoom(r);
        }
    }

    private Room getOtherRoom(Object currentRoom) {
        if(){

        }
    }


    @Override
    public void setTarget(boolean b) {

    }


    public boolean isOpen(){
        return this.open;
    }
    public void open(){
        this.open = true;
    }

}
