package Laberinto.models;

public class StandardMazeBuilder implements MazeBuilder{
    private Maze maze = new Maze();

    @Override
    public void buildRoom(int nroom) {
        Room room = new Room(nroom);
        room.setSide(Maze.Directions.North, new Wall());
        room.setSide(Maze.Directions.South, new Wall());
        room.setSide(Maze.Directions.East, new Wall());
        room.setSide(Maze.Directions.West, new Wall());
    }

    @Override
    public void setTarget(int nroom) {
        this.maze.getRoom(nroom).setTarget(true);
    }

    @Override
    public void buildDoor(int roomFloor, int roomTo, Maze.Directions dir) {
        Door d = buildDoorInternal(roomFloor,roomTo,dir);
        d = open(d);
    }

    private Door buildDoorInternal(int roomFloor, int roomTo, Maze.Directions dir) {
        Room r1 = maze.getRoom(roomFloor);
        Room r2 = maze.getRoom(roomTo);
        Door door = new Door(r1,r2);
        r1.setSide(dir,door);
        r2.setSide(getOppositeSite(dir),door);
        return door;
    }

    private Maze.Directions getOppositeSite(Maze.Directions dir) {
        switch (dir){
            case North:return Maze.Directions.South;
            case South:return Maze.Directions.North;
            case East:return Maze.Directions.West;
            case West:return Maze.Directions.East;
        }
    }

    @Override
    public void buildDoor(int roomFloor, int roomTo, Maze.Directions dir, Key key) {
        Door door = buildDoorInternal(roomFloor,roomTo,dir);
        key.addDoor(door);
    }

    @Override
    public void putKeyInRoom(int nroom, Key key) {

    }

    @Override
    public Maze getMaze() {
        return null;
    }
}
