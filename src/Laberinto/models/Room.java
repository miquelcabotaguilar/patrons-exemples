package Laberinto.models;


public class Room implements Mapside {
    private final int number;
    private Item item;
    private boolean targuet=false;

    public int getNumber() {
        return number;
    }

    public Room(int number){
        this.number=number;
    }

    public Mapside getSide(Maze.Directions dir){
        return this.sides.get(dir);
    }

    public void setSide(Maze.Directions dir,Mapside ms){
        return this.setSide(dir,ms);
    }

    @Override
    public void enter(Player player) {
        if(this.item != null){
            player.addItem(this.item);
            this.item = null;
            System.out.println("Has obtengut un item: "+this.item.toString());
        }
    }

    @Override
    public void setTarget(boolean b) {

    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public boolean isTarguet() {
        return targuet;
    }

    public void setTarguet(boolean targuet) {
        this.targuet = targuet;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + number +
                ", item=" + item +
                ", targuet=" + targuet +
                '}';
    }
}
