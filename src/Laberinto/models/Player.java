package Laberinto.models;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private Room currentRoom;
    private List<Item> itemList = new ArrayList<>();

    public void setCurrentRoom(Room room){
        System.out.println("Ets a la habitacio "+currentRoom.getNumber());
        this.currentRoom = currentRoom;
        currentRoom.enter(this);
    }

    public void addItem(Item it){
        this.itemList.add(it);
    }


    public List<Item> getItemList() {
        return itemList;
    }

    public Object getCurrentRoom() {
        return currentRoom.getNumber();
    }
}
