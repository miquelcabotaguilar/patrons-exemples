package Laberinto.models;

public interface MazeBuilder{
    void buildRoom(int nroom);
    void setTarget(int nroom);
    void buildDoor(int roomFloor,int roomTo,Maze.Directions dir);
    void buildDoor(int roomFloor,int roomTo,Maze.Directions dir,Key key);
    void putKeyInRoom(int nroom,Key key);
    Maze getMaze();
}
