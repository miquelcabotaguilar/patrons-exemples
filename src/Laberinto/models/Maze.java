package Laberinto.models;

import java.util.HashMap;
import java.util.Map;

public class Maze {

    Map<Integer,Room> rooms = new HashMap<>();

    public enum Directions{
        North,South,East,West
    }

    public void setRoom(int nroom, Room room){
        this.rooms.put(nroom, room);
    }

    public  Room getRoom(int nroom){
        return this.rooms.get(nroom);
    }


}

