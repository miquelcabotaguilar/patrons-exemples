package Laberinto;

import Laberinto.models.*;

import java.util.Scanner;
import java.util.stream.IntStream;


public class MazeGame {
    private static Scanner scanner= new Scanner(System.in);
    public static void main(String[] args) {
        Maze maze = createMaze();
        Player player= new Player();
        play(maze,player);
    }

    private static void play(Maze maze, Player player) {
        player.setCurrentRoom(maze.getRoom(1));
        while (!player.getCurrentRoom().isTarget()){
            Maze.Directions dir = askUser();
            go(player,dir);
        }
    }

    private static Maze.Directions askUser() {
        return null;
    }

    private static void go(Player player,Maze.Directions dir){
        Room room = (Room) player.getCurrentRoom();
        Mapside ms = room.getSide(dir);

    }

    private static Maze.Directions.askUser(){
        System.out.println("Cap On Vols Anar?");
        while (true){
            String line = scanner.nextLine();

        }
    }


    private static Maze createMaze() {
        MazeBuilder mazeBuilder = new StandardMazeBuilder();
        IntStream
                .range(1,7)
                .forEach(mazeBuilder::buildRoom);

        Key k1 = new Key("Level 1 key");
        Key k2 = new Key("Level 2 key");

        mazeBuilder.buildDoor(1,2,Maze.Directions.North);
        mazeBuilder.buildDoor(1,4, Maze.Directions.South);
        mazeBuilder.buildDoor(1,5, Maze.Directions.East);

        mazeBuilder.buildDoor(1,3, Maze.Directions.West,k2);
        mazeBuilder.buildDoor(5,6, Maze.Directions.East,k1);

        mazeBuilder.putKeyInRoom(6,k2);
        mazeBuilder.putKeyInRoom(2,k1);

        mazeBuilder.setTarget(3);



        return (Maze) mazeBuilder;
    }


}
